import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../service/auth.service";
import { Router } from '@angular/router';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginInfo = {password: '', username: ''};
    error_label = false;
    constructor(private auth: AuthService, private router: Router) {
    }


    ngOnInit() {
    }

    login() {
        this.auth.loginUser(this.loginInfo)
            .subscribe(
                res => {
                    this.auth.authenticated = true;
                    this.router.navigate(['home/dashboard']);
                    localStorage.setItem('token', res.jwt);
                },
                err => {
                    console.log(err);
                    this.error_label = true;
                }
            );
    }

}
