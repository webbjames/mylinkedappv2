import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {HomeRoutingModule} from "./home-routing.module";
import { DashboardComponent} from "./dashboard/dashboard.component";

import { FooterComponent } from '../partials/footer/footer.component';
import { NavbarComponent } from '../partials/navbar/navbar.component';
import { SidebarComponent } from '../partials/sidebar/sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    NgbModule.forRoot()
  ],
  declarations: [
      DashboardComponent,
      NavbarComponent,
      SidebarComponent,
      FooterComponent]
})
export class HomeModule { }
