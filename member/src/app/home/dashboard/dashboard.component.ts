import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {
    console.log(localStorage.getItem('token'));
    if(localStorage.getItem('token') == null){
        this.router.navigate(['/auth/login']);
    }
  }


}
