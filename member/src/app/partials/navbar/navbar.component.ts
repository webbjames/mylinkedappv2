import {Component, OnInit} from '@angular/core';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router";
import {AuthService} from "../../service/auth.service";

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
    providers: [NgbDropdownConfig]
})
export class NavbarComponent implements OnInit {

    public sidebarOpened = false;

    toggleOffcanvas() {
        this.sidebarOpened = !this.sidebarOpened;
        if (this.sidebarOpened) {
            document.querySelector('.sidebar-offcanvas').classList.add('active');
        }
        else {
            document.querySelector('.sidebar-offcanvas').classList.remove('active');
        }
    }

    public iconOnlyToggled = false;

    toggleIconOnlySidebar() {
        this.iconOnlyToggled = !this.iconOnlyToggled;
        if (this.iconOnlyToggled) {
            document.querySelector('body').classList.add('sidebar-icon-only');
        }
        else {
            document.querySelector('body').classList.remove('sidebar-icon-only');
        }
    }

    constructor(config: NgbDropdownConfig, private router: Router, private auth: AuthService) {
        config.placement = 'bottom-right';
    }

    ngOnInit() {
    }

    logout(e) {
        localStorage.clear();
        this.auth.authenticated = false;
        this.router.navigate(['auth/login']);
    }
}
