<?php
use Doctrine\ORM\Tools\Setup;

require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotation Mapping

$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"), $isDevMode);

// or if you prefer yaml or XML
//$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);
//$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);

// database configuration parameters
//$conn = array(
//    'driver' => 'pdo_sqlite',
//    'path' => __DIR__ . '/db.sqlite',
//);

//$conn = [
//    'dbname' => 'jamesdev',
//    'user' => 'jamesdev',
//    'password' => 'audel6209',
//    'host' => 'http://18.221.21.6/',
//    'driver' => 'mysqli'
//];

$conn = [
    'dbname' => 'spen',
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'driver' => 'mysqli'
];
// obtaining the entity manager
$entityManager = \Doctrine\ORM\EntityManager::create($conn, $config);
$connection = $entityManager->getConnection();
