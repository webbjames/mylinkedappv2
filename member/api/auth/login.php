<?php
include('../header.php');
require_once "../bootstrap.php";
require '../jwt_vendor/autoload.php';

use \Firebase\JWT\JWT;

$json = file_get_contents('php://input');
$post = json_decode($json, true);


$sql = "Select * from user where username='".$post['username']."' and  password='".md5($post['password'])."' and role = 0";
$statement = $connection->prepare($sql);
$statement->execute();
$results = $statement->fetchAll();

if(count($results) == 1){
    $token_payload = [
        'username' => $results[0]['username'],
        'role' => $results[0]['role']
    ];

// This is your client secret
    $key = 'secretkey';

// This is your id token
    $jwt = JWT::encode($token_payload, $key, 'HS256');

    http_response_code(200);
    echo json_encode(
        array(
            "data" => $results,
            "jwt" => $jwt
        )
    );
}
else{
    http_response_code(401);
    echo json_encode(array("message" => "Login failed."));
}
