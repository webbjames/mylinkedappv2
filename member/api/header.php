<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
header("Content-Type: text/plain");
if($_SERVER['REQUEST_METHOD'] == "OPTIONS"){
    exit(0);
}