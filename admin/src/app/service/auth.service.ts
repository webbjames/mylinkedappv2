import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpClientModule, HttpClientXsrfModule, HttpHeaders, HttpResponse} from '@angular/common/http';
import {  Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
    base_url = "http://localhost/api";
    // base_url = "http://localhost/api";
    authenticated = false;
    loginUser(user) {
        return this.http.post<any>(`${this.base_url}/auth/login.php`, user);
    }
    registerUser(user) {
        return this.http.post<any>(`${this.base_url}/auth/register.php`, user);
    }
}
