import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


import { AuthGaurd } from './service/auth.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'auth/login',
        pathMatch: 'full'
    },
    { path: "auth", loadChildren: "./auth/auth.module#AuthModule" },
    { path: 'home', canActivate: [AuthGaurd], loadChildren: "./home/home.module#HomeModule"}

];

@NgModule({
    imports: [RouterModule.forRoot(routes,{useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
