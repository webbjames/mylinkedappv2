import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  constructor() { }
    dashboardDropdownOpen = false;
    appsDropdownOpen = false;
    BEDropdownOpen = false;
    ADDropdownOpen = false;
    ChartsDropdownOpen = false;
    UserPageDropdownOpen = false;
    ErrorPageDropdownOpen = false;
    EcommerceDropdownOpen = false;
    iconsDropdownOpen = false;
  ngOnInit() {
  }

}
