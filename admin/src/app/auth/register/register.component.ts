import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../service/auth.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from '../../helper/must-match.validator';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  constructor(private auth: AuthService, private router: Router,  private formBuilder: FormBuilder) { }

  get f() { return this.registerForm.controls; }
  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', [Validators.required, Validators.minLength(6)]],
          confirmPassword: ['', Validators.required]
      }, {
          validator: MustMatch('password', 'confirmPassword')
      });
  }

  register(registerUserData){
      this.auth.registerUser(registerUserData)
        .subscribe(
          res => {
              if(res.msg == "success"){
                  alert("saved successfully");
                  this.router.navigate(['login']);
              }
          },
          err => {
              alert("failed to signup");
              // if (err === 'Method Not Allowed') {
              //     this.out_maxSignupCount = true;
              // }
              // if (err === 'Bad Request') {
              //     this.email_duplicate = true;
              // }
          });
  }

  onRegister(){
      this.submitted = true;
      if (this.registerForm.invalid) {
          return;
      }
      this.register(this.registerForm.value);
  }
}
